FROM mcr.microsoft.com/windows/servercore:ltsc2019

SHELL ["powershell.exe", "-ExecutionPolicy", "Bypass", "-Command"]

ENV chocolateyUseWindowsCompression=false \
    PYTHONIOENCODING=UTF-8

RUN iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')); \
    $env:Path += '";C:\tools\python3;C:\tools\python3\Scripts"';

RUN choco install --yes visualstudio2019buildtools --version=16.8.5.0; \
    if ("$LASTEXITCODE" -eq 3010) { exit 0 }
RUN choco install --yes visualstudio2019-workload-vctools --version=1.0.0 --params '"--passive --no-includeRecommended"'; \
    if ("$LASTEXITCODE" -eq 3010) { exit 0 }
RUN choco install --yes --execution-timeout=0 visualstudio2019-workload-manageddesktopbuildtools --version=1.0.1; \
    if ("$LASTEXITCODE" -eq 3010) { exit 0 }

RUN choco install --yes git --version=2.19.0 --params '"/InstallDir:C:\tools\git"'; \
    choco install --yes cmake --version=3.12.2 --params '"/InstallDir:C:\tools\cmake"' --installargs 'ADD_CMAKE_TO_PATH=""System""'; \
    choco install --yes python3 --version=3.7.0 --params '"/InstallDir:C:\tools\python3"'

RUN python -m pip install -upgrade pip; \
    python -m pip install win-unicode-console --upgrade --force-reinstall --no-cache; \
    python -m pip install conan --upgrade --force-reinstall --no-cache; \
    python -m pip install conan_package_tools --upgrade --force-reinstall --no-cache

WORKDIR "C:/Users/ContainerAdministrator"
ENTRYPOINT ["cmd.exe", "C:\\Program Files (x86)\\Microsoft Visual C++ Build Tools\\vcbuildtools_msbuild.bat"]